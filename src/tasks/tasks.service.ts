import {Injectable, NotFoundException} from '@nestjs/common';
import {Task, TaskStatus} from './tasks.model';
import * as uuid from 'uuid';
import {CreateTaskDto} from './dto/create-task.dto';
// import {UpdateTaskDto} from './dto/update-task.dto';
import {GetTasksFilterDto} from 'src/tasks/dto/get-task-filter.dto';

@Injectable()
export class TasksService {
  private tasks: Task[] = [];

  getAllTasks(): Task[] {
    return this.tasks;
  }

  // TODO: Ver como puedo refactorizar esta funcion
  getTasksWithFilter(tasksDto: GetTasksFilterDto): Task[] {
    const {status, search} = tasksDto;
    let tasks = this.getAllTasks();
    if (status) {
      tasks = tasks.filter(task => task.status === status);
    }
    if (search) {
      tasks = tasks.filter(task =>
        task.title.includes(search) ||
        task.description.includes(search));
    }
    return tasks;
  }

  getTaskById(id: string): Task {
    const found = this.tasks.find(task => task.id === id);
    if (!found) {
      throw new NotFoundException(`Task with id "${id}" not found`);
    }
    return found;
  }

  createTask(createTaskDto: CreateTaskDto): Task {
    const task = {
      ...createTaskDto,
      status: TaskStatus.OPEN,
      id: uuid.v4(),
    };

    this.tasks.push(task);
    return task;
  }

  deleteTask(id: string): void {
    const found = this.getTaskById(id);
    if (!found) {
      throw new NotFoundException(`Task with id "${id}" not found.`);
    }
    this.tasks = this.tasks.filter(task => task.id !== found.id);

  }

  updateTask(id: string, status: TaskStatus): Task {
    const task = this.getTaskById(id);
    task.status = status;
    return task;
  }
}
